<?php

class Fractions
{
    private $data    = [];
    private $coerced = [];

    /**
     * Fractions constructor.
     */
    public function __construct()
    {
        $this->data['numerator']   = 0;
        $this->data['denominator'] = 1;
    }

    /**
     * @param Fractions $a
     * @param Fractions $b
     */
    public static function coerced(Fractions $a, Fractions $b)
    {
        $a->coerced['denominator'] = $a->data['denominator'] * $b->data['denominator'];
        $b->coerced['denominator'] = $a->data['denominator'] * $b->data['denominator'];
        $a->coerced['numerator']   = $a->data['numerator'] * $b->data['denominator'];
        $b->coerced['numerator']   = $b->data['numerator'] * $a->data['denominator'];
    }

    /**
     * @param     $name
     * @param int $val
     * @throws Exception
     */
    public function __set($name, int $val)
    {
        if ($name == 'denominator' && $val == 0) throw new Exception('Знаменатель не может быть 0');
        $this->data[$name] = $val;
    }

    /**
     * @param $name
     * @return mixed|null
     */
    public function __get($name)
    {
        if (array_key_exists($name, $this->data)) {
            return $this->data[$name];
        }

        $trace = debug_backtrace();
        trigger_error(
            'Неопределённое свойство в __get(): ' . $name .
            ' в файле ' . $trace[0]['file'] .
            ' на строке ' . $trace[0]['line'],
            E_USER_NOTICE);

        return null;
    }

    /**
     * @param Fractions $a
     * @param Fractions $b
     * @return Fractions
     */
    private static function plus(Fractions $a, Fractions $b)
    : self
    {
        self::coerced($a, $b);
        $result                      = new self();
        $result->data['denominator'] = $a->coerced['denominator'];
        $result->data['numerator']   = $a->coerced['numerator'] + $b->coerced['numerator'];

        return $result;
    }

    /**
     * @param Fractions $a
     * @param Fractions $b
     * @return Fractions
     */
    private static function multiplie(Fractions $a, Fractions $b)
    : self
    {
        $result                      = new self();
        $result->data['denominator'] = $a->coerced['denominator'] * $b->coerced['denominator'];
        $result->data['numerator']   = $a->coerced['numerator'] * $b->coerced['numerator'];

        return $result;
    }

    /**
     * @param Fractions $a
     * @param Fractions $b
     * @return Fractions
     */
    private static function divide(Fractions $a, Fractions $b)
    : self
    {
        $result                      = new self();
        $result->data['denominator'] = $a->coerced['denominator'] * $b->coerced['numerator'];
        $result->data['numerator']   = $a->coerced['numerator'] * $b->coerced['denominator'];

        return $result;
    }

    /**
     * @param Fractions $a
     * @param Fractions $b
     * @return Fractions
     */
    private static function minus(Fractions $a, Fractions $b)
    : self
    {
        self::coerced($a, $b);
        $result                      = new self();
        $result->data['denominator'] = $a->coerced['denominator'];
        $result->data['numerator']   = $a->coerced['numerator'] - $b->coerced['numerator'];

        return $result;
    }

    /**
     * @param Fractions $a
     * @param Fractions $b
     * @param string    $operand
     * @return static
     * @throws Exception
     */
    public static function math(Fractions $a, Fractions $b, string $operand)
    : self
    {
        switch ($operand) {
            case '+':
                return self::plus($a, $b);
                break;
            case '-':
                return self::minus($a, $b);
                break;
            case '*':
                return self::multiplie($a, $b);
                break;
            case '/':
                return self::divide($a, $b);
                break;
            default:
                throw new Exception('Неверная операнда. Поддерживается +,-, *,/ ');
        }

    }

    /**
     * @return array
     */
    public function __serialize()
    : array
    {
        return [
            'numerator'   => $this->data['numerator'],
            'denominator' => $this->data['denominator'],
        ];
    }

    /**
     * @param array $data
     */
    public function __unserialize(array $data)
    : void
    {
        $this->data['numerator']   = $data['numerator'];
        $this->data['denominator'] = $data['denominator'];
    }

    /**
     * @return false|string
     */
    public function to_json()
    : string
    {
        return json_encode([
            'numerator'   => $this->data['numerator'],
            'denominator' => $this->data['denominator'],
        ]);
    }

    public function __toString()
    : string
    {
        return strval($this->data['numerator'] / $this->data['denominator']);
    }

}

$a              = new Fractions();
$b              = new Fractions();
$a->numerator   = 1;
$a->denominator = 2;
$b->numerator   = 7;
$b->denominator = 3;
echo Fractions::math($a, $b, '+')->to_json();
echo Fractions::math($a, $b, '+');



